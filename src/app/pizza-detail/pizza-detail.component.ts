import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pizza } from '../Models/Pizza';

@Component({
  selector: 'app-pizza-detail',
  templateUrl: './pizza-detail.component.html',
  styleUrls: ['./pizza-detail.component.scss']
})
export class PizzaDetailComponent implements OnInit {

  @Input() public pizza:Pizza;
  @Output('onOrder') onOrder = new EventEmitter<Pizza>();

  constructor() { }

  ngOnInit(): void { }

  // Event levé vers le parent
  onClickCommander() 
  { 
    this.onOrder.emit(this.pizza); 
  }

}

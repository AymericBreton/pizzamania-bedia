import { Component, OnInit } from '@angular/core';
import { LogsService } from '../logs.service';
import { Pizza } from '../Models/Pizza';
import { PizzaService } from '../pizza-service.service';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.scss']
})
export class HistoriqueComponent implements OnInit 
{
  public historiquePizzas: Array<Pizza> = new Array<Pizza>()

  public isLoading: boolean
  public isError: boolean;

  constructor(private pizzaService: PizzaService, 
              private logsService: LogsService) 
  { 
    this.isLoading = false;
    this.isError = false;
  }

  ngOnInit(): void 
  {
    this.isLoading = true;
    this.getHistorique()
    this.logsService.envoyerInfo("[OK] Arrivée sur l'historique des commandes")
  }

  // Récupération de l'historique
  getHistorique()
  {
    this.pizzaService.getHistoriqueCommandes().subscribe( 
      (res) => 
      { 
        this.isLoading = false
        this.historiquePizzas = res
        this.historiquePizzas.sort(
          (a, b) => 
          {
            let date1 = new Date(a.date)
            let date2 = new Date(b.date)
            if(date1 < date2)
              return 1
            else if(date1 > date2)
              return -1
            else
              return 0
          }
        )
        this.logsService.envoyerInfo("[OK] Récupération de l'historique des commandes de pizzas")
      },  
      (error) => 
      { 
        this.isError = true
        this.logsService.envoyerError("[ECHEC] Echec lors de la récupération de l'historique des commandes de pizzas." + error.message)
      })  
  }
}

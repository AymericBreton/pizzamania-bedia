import { Component, OnInit, OnDestroy } from '@angular/core';
import { LogsService } from '../logs.service';
import { Pizza } from '../Models/Pizza';
import { PizzaService } from '../pizza-service.service';
import { StockageService } from '../stockage.service';
import { ResponsePizza } from '../Models/ResponsePizza';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit, OnDestroy {

  public lastPizza: Pizza;
  public pizza: Pizza
  public commandDone: boolean
  public reponseCommande: ResponsePizza
  public isLoadingCommand: boolean
  public isErrorCommand: boolean;

  constructor(private pizzaService:PizzaService, 
              private stockageService: StockageService, 
              private logsService: LogsService, 
              private router:Router) 
  {
    this.isLoadingCommand = false
    this.commandDone = false
    this.isErrorCommand = false
    
  }

  ngOnInit(): void 
  {
    this.lastPizza = this.stockageService.getPizza()
    this.logsService.envoyerInfo("[OK] Arrivée sur la page d'accueil")
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
  }

  ngOnDestroy()
  {
    this.router.onSameUrlNavigation = 'ignore'
  }

  // Commande de la dernière pizza commandée
  public commanderPizza(pizza: Pizza) 
  { 
    this.isLoadingCommand = true
    this.pizza = pizza

    this.pizzaService.postCommanderPizza(this.pizza).subscribe( 
      (res) => 
      { 
        this.reponseCommande = res
        this.isLoadingCommand = false
        this.commandDone = true
        this.logsService.envoyerInfo("[OK] Commande de la pizza depuis accueil")
      },  
      (error) => 
      { 
        this.isErrorCommand = true
        this.logsService.envoyerError("[ECHEC] Erreur dans la commande d'une pizza depuis accueil;" + error.message)
      }
    )
  }
  
  // Remise à 0
  public retourAccueil() 
  {
    this.commandDone = false
    this.isLoadingCommand = false
    this.isErrorCommand = false
  }
}

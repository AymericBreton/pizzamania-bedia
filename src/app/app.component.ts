import { Component } from '@angular/core';
import { Pizza } from './Models/Pizza';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent 
{
  title = 'pizzamania'
} 
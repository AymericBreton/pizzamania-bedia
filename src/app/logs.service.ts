import { Injectable } from '@angular/core'; 
import { HttpClient } from "@angular/common/http"; 


@Injectable({
  providedIn: 'root'
})
export class LogsService 
{

  private url = 'https://pizzamania-quarkus.ew.r.appspot.com'

  constructor(private http: HttpClient) {}

  public envoyerInfo(message: string)
  {
    this.http.get<any[]>(this.url + "/logger/info?message=" + message).subscribe()  
  }

  public envoyerWarning(message: string)
  {
    this.http.get<any[]>(this.url + "/logger/warn?message=" + message).subscribe()
  }

  public envoyerError(message: string)
  {
    this.http.get<any[]>(this.url + "/logger/error?message=" + message).subscribe()
  }
}


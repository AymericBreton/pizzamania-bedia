import { Component, OnInit } from '@angular/core';
import { PizzaService } from '../pizza-service.service';
import { Pizza } from '../Models/Pizza';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { StockageService } from '../stockage.service';
import { LogsService } from '../logs.service';
import { ResponsePizza } from '../Models/ResponsePizza';

@Component({
  selector: 'app-boutique-pizza',
  templateUrl: './boutique-pizza.component.html',
  styleUrls: ['./boutique-pizza.component.scss']
})
export class BoutiquePizzaComponent implements OnInit {

  public pizza: Pizza
  public isLoading: boolean
  public isSuccess: boolean
  public isError: boolean
  public commandDone: boolean
  public lesPizzas: Array<Pizza> = new Array<Pizza>()
  public reponseCommande: ResponsePizza

  public isLoadingCommand: boolean
  public isErrorCommand: boolean

  public prixMax: number

  constructor(private pizzaService:PizzaService, 
              private stockageService: StockageService, 
              private logsService: LogsService) 
  {
    this.commandDone = false
    this.isLoadingCommand = false
    this.isErrorCommand = false

    this.prixMax = 10
  }

  ngOnInit(): void 
  {
    this.isLoading = false
    this.callPizzaService()
    this.logsService.envoyerInfo("[OK] Arrivée sur la boutique pizza")
  }

  // Service pour récupérer les pizzas
  public callPizzaService() 
  { 
    this.isLoading = true

    this.pizzaService.getPizzasBoutique().subscribe( 
      (res) => 
      { 
        this.onSuccess(res)
      },  
      (error) => 
      { 
        this.onError(error)
      }
    )  
  } 
 
  // Pizzas récupérées
  public onSuccess(pizzas: Array<Pizza>) 
  { 
    this.isLoading = false

    pizzas.forEach(element => 
    { 
      this.lesPizzas.push(element)
    })

    this.isSuccess = true
    this.logsService.envoyerInfo("[OK] Récupération de la boutique de pizza depuis le serveur effectuée")
  } 

  // Pizzas pas récupérées
  public onError(err:HttpErrorResponse) 
  { 
    this.isLoading = false
    this.isError = false

    this.logsService.envoyerError("[ECHEC] Erreur lors de la récupération de la boutique de pizza depuis le serveur")
  }

  // Commande d'une pizza, event venant de l'enfant
  public commanderPizza(pizza: Pizza)
  {
    this.isLoadingCommand = true

    this.pizzaService.postCommanderPizza(pizza).subscribe( 
      (res) => { 
        this.reponseCommande = res
        this.pizza = pizza
        this.isLoadingCommand = false
        this.commandDone = true
        this.logsService.envoyerInfo("[OK] Commande de la pizza depuis la boutique")

        this.stockageService.putPizza(this.pizza)
        this.logsService.envoyerWarning("[WARN] Ajout d'une pizza dans le local storage")
      },  
      (error) => 
      { 
        this.isErrorCommand = true

        this.logsService.envoyerError("[ECHEC] Erreur lors de la commande d'une pizza depuis la boutique de pizza." + error.message)
      }
    )
  }
  
  // Remise à 0 de la page
  public retourBoutique() 
  {
    this.commandDone = false
    this.isLoadingCommand = false
    this.isErrorCommand = false
  }

}

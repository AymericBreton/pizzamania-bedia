import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoutiquePizzaComponent } from './boutique-pizza.component';

describe('BoutiquePizzaComponent', () => {
  let component: BoutiquePizzaComponent;
  let fixture: ComponentFixture<BoutiquePizzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoutiquePizzaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoutiquePizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

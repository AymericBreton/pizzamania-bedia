import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pizza } from './Models/Pizza';

@Injectable({
  providedIn: 'root'
})
export class StockageService {

  constructor(private http: HttpClient) {}

  // Mise de la dernière pizza dans le stockage local
  public putPizza(pizza:Pizza)
  {
    window.localStorage.setItem('lastPizza', JSON.stringify(pizza))
  }

  // Récupération de la dernière pizza du stockage local
  public getPizza(): Pizza
  {
    return JSON.parse(window.localStorage.getItem('lastPizza'))
  }
}

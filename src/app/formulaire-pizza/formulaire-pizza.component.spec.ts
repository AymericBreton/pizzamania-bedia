import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulairePizzaComponent } from './formulaire-pizza.component';

describe('FormulairePizzaComponent', () => {
  let component: FormulairePizzaComponent;
  let fixture: ComponentFixture<FormulairePizzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulairePizzaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulairePizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

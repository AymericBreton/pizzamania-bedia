import { JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogsService } from '../logs.service';
import { Pizza } from '../Models/Pizza';
import { PizzaService } from '../pizza-service.service';
import { StockageService } from '../stockage.service';
import { ResponsePizza } from '../Models/ResponsePizza';

@Component({
  selector: 'app-formulaire-pizza',
  templateUrl: './formulaire-pizza.component.html',
  styleUrls: ['./formulaire-pizza.component.scss']
})

export class FormulairePizzaComponent implements OnInit 
{
  public commandDone: boolean
  public reponseCommande: ResponsePizza
  public isLoadingCommand: boolean
  public isErrorCommand: boolean

  pizza: Pizza

  constructor(private pizzaService:PizzaService, 
              private stockageService: StockageService, 
              private logsService: LogsService) 
  { 
    this.pizza = new Pizza()
    this.pizza.image = "/assets/images/pizza-a-composer.png"
    this.pizza.base = this.pizza.lesBases[0].nom
    this.pizza.pate = this.pizza.lesPates[0]
    this.commandDone = false
    this.isLoadingCommand = false   
  }

  ngOnInit(): void 
  {
    this.logsService.envoyerInfo("[OK] Arrivée sur le formulaire de commande d'une pizza")
  }

  public getCommandeValider(): boolean 
  {
    return this.pizza.auMoinsUnIngredients()
  }

  // Commande de la pizza
  public commanderPizza(pizza: Pizza) 
  {
    this.isLoadingCommand = true

    this.pizzaService.postCommanderPizza(pizza).subscribe( 
      (res) => 
      { 
        this.reponseCommande = res
        this.pizza = pizza
        this.isLoadingCommand = false
        this.commandDone = true
        this.logsService.envoyerInfo("[OK] Commande de la pizza depuis le formulaire de pizza")

        this.stockageService.putPizza(JSON.parse(this.pizza.toJson() as string))
        this.logsService.envoyerWarning("[WARN] Ajout d'une pizza dans le local storage")
      },  
      (error) => 
      { 
        this.isErrorCommand = true
        this.logsService.envoyerError("[ECHEC] Erreur lors de la commande d'une pizza depuis le formulaire de pizza." + error.message)
      }
    )
  }
  
  // Remise à 0 de la page
  public retourFormulaire() 
  {
    this.commandDone = false
    this.isLoadingCommand = false
    this.isErrorCommand = false
  }
}

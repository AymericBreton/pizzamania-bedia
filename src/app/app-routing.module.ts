import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { BoutiquePizzaComponent } from './boutique-pizza/boutique-pizza.component';
import { ConfirmationCommandeComponent } from './confirmation-commande/confirmation-commande.component';
import { FormulairePizzaComponent } from './formulaire-pizza/formulaire-pizza.component';
import { HistoriqueComponent } from './historique/historique.component';

const routes: Routes = [
  {path: '', component: AccueilComponent},
  {path: 'personnaliser-pizza', component: FormulairePizzaComponent},
  {path: 'confirmer-commande', component: ConfirmationCommandeComponent},
  {path: 'boutique-pizza', component: BoutiquePizzaComponent},
  {path: 'historique', component: HistoriqueComponent},
  {path: '**', component: AccueilComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 
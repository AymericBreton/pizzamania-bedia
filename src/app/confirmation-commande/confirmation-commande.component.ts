import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LogsService } from '../logs.service';
import { Pizza } from '../Models/Pizza';
import { ResponsePizza } from '../Models/ResponsePizza';

@Component({
  selector: 'app-confirmation-commande',
  templateUrl: './confirmation-commande.component.html',
  styleUrls: ['./confirmation-commande.component.scss']
})

export class ConfirmationCommandeComponent implements OnInit {


  public pizzaJson: Pizza

  @Input() public pizza:Pizza
  @Input() public reponseCommande:ResponsePizza
 
  constructor(private logsService: LogsService) { }

  ngOnInit(): void {  
    this.pizzaJson = this.pizza
    this.pizzaJson.prix = this.pizzaJson.getPrix()

    this.logsService.envoyerInfo("[OK] Arrivée sur la confirmation de commande")
  }

}

// Enum avec le prix des ingrédients
enum Ingredients {ANCHOIS = 1, MIEL = 3, JAMBON = 2, MAGRET = 4}

export class Pizza 
{
    public nom: string
    public pate: string
    public base: string
    public image: string

    public Ingredients = Ingredients

    public miel: boolean
    public anchois: boolean
    public jambon: boolean
    public magret: boolean
    public date: Date
    public prix: number

    public lesBases = [
        {
            nom: "tomate",
            prix: 3
        },
        { 
            nom: "creme",
            prix: 4
        }
    ]
    
    public lesPates = [
        "Fine",
        "Epaisse"
    ]

    constructor() { }

    // Validation de la présence d'au moins un ingrédient
    public auMoinsUnIngredients(): boolean 
    {
        if (this.anchois)
            return true
        if (this.jambon)
            return true
        if (this.magret)
            return true
        if (this.miel)
            return true

        return false
    }

    // Prix total de la pizza
    public getPrix(): number 
    {
        let prix = 0

        this.lesBases.forEach(base => {
            if (this.base == base.nom)
            {
                prix += base.prix
            }
        })
       
        if (this.anchois)
            prix += Ingredients.ANCHOIS
        if (this.jambon)
            prix += Ingredients.JAMBON
        if (this.magret)
            prix += Ingredients.MAGRET
        if (this.miel)
            prix += Ingredients.MIEL

        return prix
    }

    public toJson(): Object 
    {
        return JSON.stringify({
            nom: this.nom,
            miel: this.miel,
            anchois: this.anchois,
            jambon: this.jambon,
            magret: this.magret,
            pate: this.pate,
            base: this.base,
            image: this.image,
            prix: this.getPrix()
        })
    }
}
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { FormulairePizzaComponent } from './formulaire-pizza/formulaire-pizza.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ConfirmationCommandeComponent } from './confirmation-commande/confirmation-commande.component';
import { BoutiquePizzaComponent } from './boutique-pizza/boutique-pizza.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { HttpClientModule } from '@angular/common/http';
import { HistoriqueComponent } from './historique/historique.component';
import { TransformTimePipe } from './transform-time.pipe';
import { PizzaDetailComponent } from './pizza-detail/pizza-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    FormulairePizzaComponent,
    AccueilComponent,
    ConfirmationCommandeComponent,
    BoutiquePizzaComponent,
    SpinnerComponent,
    HistoriqueComponent,
    TransformTimePipe,
    PizzaDetailComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export {PizzaService} from './pizza-service.service'

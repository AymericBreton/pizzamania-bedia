import {Injectable, OnInit} from '@angular/core'; 
import {HttpClient, HttpErrorResponse} from "@angular/common/http"; 
import {Observable} from "rxjs"; 
import {Pizza} from "./Models/Pizza"
import { ResponsePizza } from './Models/ResponsePizza';


@Injectable({
  providedIn: "root"
}) 
export class PizzaService { 

  constructor(private http: HttpClient) {}

  private url = 'https://pizzamania-quarkus.ew.r.appspot.com'; 

  // Récupération des pizzas
  public getPizzasBoutique():Observable<Pizza[]> 
  { 
    return this.http.get<Pizza[]>(this.url + "/boutique/pizzas"); 
  } 

  // Commande d'une pizza
  public postCommanderPizza(pizza: Pizza):Observable<ResponsePizza> 
  { 
    return this.http.post<ResponsePizza>(this.url + "/boutique/commanderPizza2", pizza)
  }
  
  // Historique
  public getHistoriqueCommandes(): Observable<Pizza[]>
  {
    return this.http.get<Pizza[]>(this.url + "/boutique/historique")
  }

}

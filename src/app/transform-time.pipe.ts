import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transformTime'
})
export class TransformTimePipe implements PipeTransform 
{

  // Met en forme une date
  transform(value: string): unknown 
  {
    let now = new Date()
    let dateCommande = new Date(value)
    let tempsPasseMillisecondes = now.getTime() - dateCommande.getTime()

    if (tempsPasseMillisecondes < 0) 
    {
      return "Une commande qui vient du futur ???"
    }
    else
    {
      let tempsPasse = convertisseurMillisecondes(tempsPasseMillisecondes)

      return "Il y a " + tempsPasse;
    }
   
  }
}

// Converti des millisecondes en J/H/M/S
function convertisseurMillisecondes(tempsPasseMillisecondes: number): string 
{
  let secondes = Math.floor(tempsPasseMillisecondes/1000)
  let res = ""
  if (secondes > 59) 
  {
    let minutes = Math.floor(secondes/60)
    secondes = secondes % 60
    if (minutes > 59) 
    {
      let heures = Math.floor(minutes / 60)
      minutes = minutes % 60
      if (heures > 24) 
      {
        let jours = Math.floor(heures / 60)
        heures = heures % 60
        res += jours + "j"
      }
      res += heures + "h"
    }
    res += minutes + "m"
  }
  res += secondes + "s"

  return res
}

